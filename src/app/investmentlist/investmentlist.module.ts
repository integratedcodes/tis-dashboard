import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InvestmentlistRoutingModule } from './investmentlist-routing.module';
import { InvestmentlistComponent } from './investmentlist.component';


@NgModule({
  declarations: [InvestmentlistComponent],
  imports: [
    CommonModule,
    InvestmentlistRoutingModule
  ]
})
export class InvestmentlistModule { }
