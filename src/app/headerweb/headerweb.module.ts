import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HeaderwebRoutingModule } from './headerweb-routing.module';
import { HeaderwebComponent } from './headerweb.component';


@NgModule({
  declarations: [HeaderwebComponent],
  imports: [
    CommonModule,
    HeaderwebRoutingModule
  ]
})
export class HeaderwebModule { }
