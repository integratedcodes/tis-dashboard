import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReplymessageRoutingModule } from './replymessage-routing.module';
import { ReplymessageComponent } from './replymessage.component';


@NgModule({
  declarations: [ReplymessageComponent],
  imports: [
    CommonModule,
    ReplymessageRoutingModule
  ]
})
export class ReplymessageModule { }
