import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OngoinginvestmentsdetailsRoutingModule } from './ongoinginvestmentsdetails-routing.module';
import { OngoinginvestmentsdetailsComponent } from './ongoinginvestmentsdetails.component';


@NgModule({
  declarations: [OngoinginvestmentsdetailsComponent],
  imports: [
    CommonModule,
    OngoinginvestmentsdetailsRoutingModule
  ]
})
export class OngoinginvestmentsdetailsModule { }
