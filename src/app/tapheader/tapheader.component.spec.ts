import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TapheaderComponent } from './tapheader.component';

describe('TapheaderComponent', () => {
  let component: TapheaderComponent;
  let fixture: ComponentFixture<TapheaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TapheaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TapheaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
