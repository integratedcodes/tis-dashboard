import { Component, OnInit, ViewContainerRef, ViewChild, Injector, ComponentFactoryResolver } from '@angular/core';
import { Router } from '@angular/router';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-headermobile',
  templateUrl: './headermobile.component.html',
  styleUrls: ['./headermobile.component.scss']
})
export class HeadermobileComponent implements OnInit {

  @ViewChild('header', { read: ViewContainerRef, static: true })
  private topheader: ViewContainerRef;


  @ViewChild('footer', { read: ViewContainerRef, static: true })
  private footercontainer: ViewContainerRef;


  showP: boolean;
  investors: Array<Object> = [{ 'name': "", "date": "", amount: '', completed: ' ' }]
  constructor(private meta: Meta, private title: Title,
    private inj: Injector, private resolser: ComponentFactoryResolver,
    private router: Router) {
    // this.router.navigate(['/tap-account-dashboard/dashboard']);
  }

  async ngOnInit() {


    if (window.sessionStorage.getItem("user") === 'tap') {
      this.title.setTitle("TIS TAP Dashboard | Dahsboard ");
      this.meta.addTag({name : "description" , content : "Tcode Investment Affliate Dashboard"});
      const { TapheaderModule } = await import('../tapheader/tapheader.module');
      const topheaderFactory1 = this.resolser.resolveComponentFactory(TapheaderModule);
      this.topheader.createComponent(topheaderFactory1, null, this.inj);
    } 
    
    
    else if (window.sessionStorage.getItem("user") === 'investor') {
      this.title.setTitle("TIS Investor Dashboard | Dahsboard ");
      this.meta.addTag({name : "description" , content : "Tcode Investment Investor Dashboard"});
      const { UserheaderModule } = await import('../userheader/userheader.module');
      const topheaderFactory2 = this.resolser.resolveComponentFactory(UserheaderModule);
      this.topheader.createComponent(topheaderFactory2, null, this.inj);
    } 
    
    
    else if (window.sessionStorage.getItem("user") === 'admin') {
      this.title.setTitle("TIS Admin Dashboard | Management Dahsboard ");
      this.meta.addTag({name : "description" , content : "Tcode Investment Admin Dashboard"});
      const { AdminheaderModule } = await import('../adminheader/adminheader.module');
      const topheaderFactory3 = this.resolser.resolveComponentFactory(AdminheaderModule);
      this.topheader.createComponent(topheaderFactory3, null, this.inj);
    };


    const { FooterComponent } = await import('../footer/footer.component');
    const footerFactory = this.resolser.resolveComponentFactory(FooterComponent);
    this.footercontainer.createComponent(footerFactory, null, this.inj);
  }
  prevent(event: Event) {
    event.preventDefault();
  }
}
