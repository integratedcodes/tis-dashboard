import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HeadermobileRoutingModule } from './headermobile-routing.module';
import { HeadermobileComponent } from './headermobile.component';


@NgModule({
  declarations: [HeadermobileComponent],
  imports: [
    CommonModule,
    HeadermobileRoutingModule
  ]
})
export class HeadermobileModule { }
