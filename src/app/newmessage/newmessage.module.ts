import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewmessageRoutingModule } from './newmessage-routing.module';
import { NewmessageComponent } from './newmessage.component';


@NgModule({
  declarations: [NewmessageComponent],
  imports: [
    CommonModule,
    NewmessageRoutingModule
  ]
})
export class NewmessageModule { }
