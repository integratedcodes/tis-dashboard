import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TopmenubarComponent } from './topmenubar.component';



@NgModule({
  declarations: [TopmenubarComponent],
  imports: [
    CommonModule
  ]
})
export class TopmenubarModule { }
