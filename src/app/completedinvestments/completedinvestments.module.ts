import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CompletedinvestmentsRoutingModule } from './completedinvestments-routing.module';
import { CompletedinvestmentsComponent } from './completedinvestments.component';


@NgModule({
  declarations: [CompletedinvestmentsComponent],
  imports: [
    CommonModule,
    CompletedinvestmentsRoutingModule
  ]
})
export class CompletedinvestmentsModule { }
