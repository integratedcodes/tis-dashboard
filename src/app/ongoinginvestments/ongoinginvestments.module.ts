import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OngoinginvestmentsRoutingModule } from './ongoinginvestments-routing.module';
import { OngoinginvestmentsComponent } from './ongoinginvestments.component';


@NgModule({
  declarations: [OngoinginvestmentsComponent],
  imports: [
    CommonModule,
    OngoinginvestmentsRoutingModule
  ]
})
export class OngoinginvestmentsModule { }
