import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ActiveinvestmentsComponent } from './activeinvestments.component';


const routes: Routes = [{
  component : ActiveinvestmentsComponent , path : "/" , canLoad : []
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ActiveinvestmentsRoutingModule { }
