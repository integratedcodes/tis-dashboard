import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginguardGuard } from '../services/loginguard.guard';
import { AdmindashboardComponent } from './admindashboard.component';


const routes: Routes = [
  {
    path: '', component: AdmindashboardComponent, canActivate: [LoginguardGuard],
    children: [
      { path: 'signin', loadChildren: () => import('../sigin/sigin-routing.module').then(m => m.SiginRoutingModule) },
      { path: 'dashboard', loadChildren: () => import('../dashboard/dashboard-module.module').then(m => m.DashboardModuleModule) },
      { path: 'dashboard/activities', loadChildren: () => import('../activities/activities-module.module').then(m => m.ActivitiesModuleModule) },
      { path: 'dashboard/editprofile/', loadChildren: () => import('../editprofile/editprofile.module').then(m => m.EditprofileModule) },
      { path: 'tapagentlist', loadChildren: () => import('../tapagentlist/tapagentlist.module').then(m => m.TapagentlistModule) },
      { path: 'tapagentlistactive', loadChildren: () => import('../tapagentprofileactive/tapagentprofileactive.module').then(m => m.TapagentprofileactiveModule) },
      { path: 'tapagentprofile', loadChildren: () => import('../tapagentprofile/tapagentprofile.module').then(m => m.TapagentprofileModule) },
      { path: 'adminuserprofile', loadChildren: () => import('../adminuserprofile/adminuserprofile.module').then(m => m.AdminuserprofileModule) },
      { path: 'resetpassword', loadChildren: () => import('../resetpassword/resetpassword.module').then(m => m.ResetpasswordModule) },
      {
        path: 'messages', loadChildren: () => import('../messages/messages.module').then(m => m.MessagesModule),
        children: [
          { path: 'messsagerecieved', loadChildren: () => import('../messagesreceived/messagesreceived.module').then(m => m.MessagesreceivedModule) },
          // { path: 'sent', loadChildren: () => import('../sent/sent.module').then(m => m.SentModule) },
          { path: 'newmessage', loadChildren: () => import('../newmessage/newmessage.module').then(m => m.NewmessageModule) },
        ]
      },

      { path: 'investments', loadChildren: () => import('../investmentlist/investmentlist.module').then(m => m.InvestmentlistModule) },
      { path: 'investments/active', loadChildren: () => import('../investmentlist/investmentlist.module').then(m => m.InvestmentlistModule) },
      { path: 'investments/details/active', loadChildren: () => import('../investmentlistdetailactive/investmentlistdetailactive.module').then(m => m.InvestmentlistdetailactiveModule) },
      { path: 'investments/completed', loadChildren: () => import('../completedinvestments/completedinvestments.module').then(m => m.CompletedinvestmentsModule) },
      { path: 'investments/completed/details', loadChildren: () => import('../completedinvestments/completedinvestments.module').then(m => m.CompletedinvestmentsModule) },
      { path: 'investments/abandoned', loadChildren: () => import('../abandonedinvestments/abandonedinvestments.module').then(m => m.AbandonedinvestmentsModule) },
      { path: 'investments/abandoned/details', loadChildren: () => import('../abandonedinvestmentsdetails/abandonedinvestmentsdetails.module').then(m => m.AbandonedinvestmentsdetailsModule) },
      { path: 'investments/ongoing/details', loadChildren: () => import('../ongoinginvestmentsdetails/ongoinginvestmentsdetails.module').then(m => m.OngoinginvestmentsdetailsModule) },
      { path: 'investments/ongoing', loadChildren: () => import('../ongoinginvestments/ongoinginvestments.module').then(m => m.OngoinginvestmentsModule) },
      { path: 'investments/unapproved', loadChildren: () => import('../unapprovedinvestments/unapprovedinvestments.module').then(m => m.UnapprovedinvestmentsModule) },
      { path: 'investments/planmanagement', loadChildren: () => import('../investmentmanagement/investmentmanagement.module').then(m => m.InvestmentmanagementModule) },
      { path: 'investments/planmanagement/editprofile', loadChildren: () => import('../editprofile/editprofile.module').then(m => m.EditprofileModule) },
      { path: 'investments/unapproved/details', loadChildren: () => import('../unapprovedinvestmentsdetails/unapprovedinvestmentsdetails.module').then(m => m.UnapprovedinvestmentsdetailsModule) },
      { path: 'investments/unapproved/details', loadChildren: () => import('../unapprovedinvestmentsdetails/unapprovedinvestmentsdetails.module').then(m => m.UnapprovedinvestmentsdetailsModule) },
      { path: 'investments/unapproved/details', loadChildren: () => import('../unapprovedinvestmentsdetails/unapprovedinvestmentsdetails.module').then(m => m.UnapprovedinvestmentsdetailsModule) },
      { path: 'investments/editbasicprofileinfo/:planName', loadChildren: () => import('../editprofilebasicinfo/editprofilebasicinfo.module').then(m => m.EditprofilebasicinfoModule) },

      // { path: 'investments/start_new_investment', loadChildren: () => import('../new-investment/new-investment.module').then(m => m.NewInvestmentModule) },
      // { path: 'investments/start_new_investment/create_new_investment', loadChildren: () => import('../create-new-investment/create-new-investment.module').then(m => m.CreateNewInvestmentModule) },
      // { path: 'investments/start_new_investment/payment_methods', loadChildren: () => import('../payment-methods/payment-methods.module').then(m => m.PaymentMethodsModule) },
      // { path: 'investments/start_new_investment/payment_methods/pay_with_bank_transfer', loadChildren: () => import('../banktransfer/banktransfer.module').then(m => m.BanktransferModule) },

    ]
  },
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdmindashboardRoutingModule { }
