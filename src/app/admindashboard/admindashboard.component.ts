import { Component, OnInit, ViewContainerRef, ViewChild, ComponentFactoryResolver, Injector } from '@angular/core';
import { Router } from '@angular/router';
import { SidemenubarModule } from '../sidemenubar/sidemenubar.module';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-admindashboard',
  templateUrl: './admindashboard.component.html',
  styleUrls: ['./admindashboard.component.scss']
})
export class AdmindashboardComponent implements OnInit {

  @ViewChild('leftsidenav', { read: ViewContainerRef, static: true })
  private leftsidenav: ViewContainerRef;

  // @ViewChild('rightsidenav', { read: ViewContainerRef, static: true })
  // private rightsidenav: ViewContainerRef;

  @ViewChild('header', { read: ViewContainerRef, static: true })
  private topheader: ViewContainerRef;


  @ViewChild('footer', { read: ViewContainerRef, static: true })
  private footercontainer: ViewContainerRef;


  showP: boolean;
  investors: Array<Object> = [{ 'name': "", "date": "", amount: '', completed: ' ' }]
  constructor(private meta: Meta, private title: Title,
    private inj: Injector, private resolser: ComponentFactoryResolver,
    private router : Router) {
      // this.router.navigate(['/tap-account-dashboard/dashboard']);
     }

  async ngOnInit() {

    this.title.setTitle("TIS Adminitrator Dashboard | TIS Account Management Dahsboard ");

    const { SidemenubarComponent } = await import('../sidemenubar/sidemenubar.component');
    const sideNaveFactory = this.resolser.resolveComponentFactory(SidemenubarComponent);
    this.leftsidenav.createComponent(sideNaveFactory, null, this.inj);

    const { TopmenubarComponent } = await import('../topmenubar/topmenubar.component');
    const rightsidenavFactory = this.resolser.resolveComponentFactory(TopmenubarComponent);
    this.topheader.createComponent(rightsidenavFactory, null, this.inj);

    const { FooterComponent } = await import('../footer/footer.component');
    const footerFactory = this.resolser.resolveComponentFactory(FooterComponent);
    this.footercontainer.createComponent(footerFactory, null, this.inj);

  }


  prevent(event: Event) {
    event.preventDefault();
  }

}
